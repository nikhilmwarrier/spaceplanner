export function getTotalEntries(db_data) {
    let count = 0;
    Object.values(db_data).forEach(box => {
        count += box.length;
    });
    return count;
}

export function getUnfinishedEntriesInDay(dayOfLoop, db_data, daysMap) {
    let finishedCount = 0;
    let totalCount = 0;
    daysMap[dayOfLoop].boxes.forEach(box => {
        if (!!db_data[box]) {
            console.log(db_data[box]);
            db_data[box].forEach(item => {
                totalCount += 1;
                if (!!item.hasFinished) finishedCount += 1;
            });
        }
    });
    console.log(totalCount, finishedCount);
    return totalCount - finishedCount;
}

export function resetFinishTracking(db_data) {
    let copy = { ...db_data };
    Object.values(copy).forEach(box => {
        box.forEach(item => (item.hasFinished = false));
    });
    return copy;
}
