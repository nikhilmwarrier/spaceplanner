import { writable } from "svelte/store";

const emptyDB = {
    a: [],
    b: [],
    c: [],
    d: [],
    e: [],
    f: [],
    g: [],
};

const defaultData = JSON.parse(localStorage.getItem("db")) || emptyDB;

export const db_data_store = writable(defaultData);
