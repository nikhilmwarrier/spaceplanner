import json
mapfile = open("daysmap.json", "w")

finalJSON = "[\n"

def boxes(actualDay):
    day = actualDay + 1         # To fix off-by-one errors, as the days in the if-statements are using days from ncase.me/remember (not zero indexed), but the final json array is zero-indexed
    finalList = ["a"]
    if day % 2 != 0:
        finalList.append("b")
    if (day - 2) % 4 == 0:
        finalList.append("c")
    if day in [4, 13, 20, 29, 36, 45, 52, 61]:
        finalList.append("d")
    if day in [12, 28, 44, 60]:
        finalList.append("e")
    if day in [24, 59]:
        finalList.append("f")
    if day == 56:
        finalList.append("g")

    return json.dumps(finalList[::-1])

for day in range(64):
    jsonToAppend = f'''    {{
        "day": {day},
        "boxes": {boxes(day)}
    }}'''
    if day != 63:
        jsonToAppend += ",\n"
    else:
        jsonToAppend += "\n]"

    finalJSON += jsonToAppend

mapfile.write(finalJSON)